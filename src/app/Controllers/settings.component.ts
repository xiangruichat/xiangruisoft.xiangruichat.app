import { Component } from '@angular/core';
import { AuthApiService } from '../Services/AuthApiService';
import { Router } from '@angular/router';
import { XiangruiChatUser } from '../Models/XiangruiChatUser';
import { Values } from '../values';
import { InitService } from '../Services/InitService';
import { MessageService } from '../Services/MessageService';
import { HeaderService } from '../Services/HeaderService';
import Swal from 'sweetalert2';

@Component({
    templateUrl: '../Views/settings.html',
    styleUrls: ['../Styles/menu.css',
        '../Styles/button.css']
})
export class SettingsComponent {
    public loadingImgURL = Values.loadingImgURL;
    constructor(
        private authApiService: AuthApiService,
        private router: Router,
        private initSerivce: InitService,
        public messageService: MessageService,
        private headerService: HeaderService) {
        this.headerService.title = '我';
        this.headerService.returnButton = false;
        this.headerService.button = false;
        this.headerService.shadow = false;
    }

    public GetMe(): XiangruiChatUser {
        return this.messageService.me;
    }

    public SignOut(): void {
        Swal({
            title: '是否要退出?',
            type: 'warning',
            showCancelButton: true
        }).then((willSignOut) => {
            if (willSignOut.value) {
                this.authApiService.LogOff().subscribe(() => {
                    this.initSerivce.destroy();
                    this.router.navigate(['/signin'], { replaceUrl: true });
                });
            }
        });
    }

    public SendReport(): void {
        Swal({
            title: '错误报告[Bug Report]',
            input: 'textarea',
            inputPlaceholder: '输入错误报告信息...',
            inputAttributes: {
                maxlength: '1000'
            },
            confirmButtonColor: 'red',
            showCancelButton: true,
            confirmButtonText: '报告'
        }).then((result) => {
            if (result.value) {
                if (result.value.length >= 2) {
                    this.authApiService.ReportBug(result.value).subscribe((response) => {
                        if (response.code === 0) {
                            Swal('成功', '您成功的反馈了这个bug', 'success');
                        } else {
                            Swal('失败', '服务器出现了错误.', 'error');
                        }
                    });
                } else {
                    Swal('失败', '报告信息的长度应在 2 - 200之间.', 'error');
                }
            }
        });
    }

    public SendReportMessage(): void {
        Swal({
            title: '反馈需求',
            input: 'textarea',
            inputPlaceholder: '输入需求反馈信息...',
            inputAttributes: {
                maxlength: '1000'
            },
            showCancelButton: true,
            confirmButtonText: '报告'
        }).then((result) => {
            if (result.value) {
                if (result.value.length >= 6) {
                    this.authApiService.ReportMessage(result.value).subscribe((response) => {
                        if (response.code === 0) {
                            Swal('成功', '您成功的反馈了这个需求', 'success');
                        } else {
                            Swal('失败', '服务器出现了错误.', 'error');
                        }
                    });
                } else {
                    Swal('失败', '报告信息的长度应在 6 - 200之间.', 'error');
                }
            }
        });
    }
}
