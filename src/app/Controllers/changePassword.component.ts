import { Component } from '@angular/core';
import { AuthApiService } from '../Services/AuthApiService';
import Swal from 'sweetalert2';
import { catchError } from 'rxjs/operators';
import { HeaderService } from '../Services/HeaderService';
import { Router } from '@angular/router';

@Component({
    templateUrl: '../Views/changePassword.html',
    styleUrls: [
      '../Styles/userDetail.css',
      '../Styles/button.css'
    ]
})
export class ChangePasswordComponent {
    public oldPassword = '';
    public newPassword = '';
    public confirmPassword = '';
    public samePassword = false;
    private valid = false;

    constructor(
        private authApiServer: AuthApiService,
        private headerService: HeaderService,
        private router: Router
    ) {
        this.headerService.title = '修改密码';
        this.headerService.returnButton = true;
        this.headerService.button = false;
        this.headerService.shadow = false;
    }

    public checkValid(): void {
        this.samePassword = this.newPassword === this.confirmPassword ? true : false;
        if (this.oldPassword.length >= 6 && this.oldPassword.length <= 32 && this.newPassword.length >= 6 &&
            this.newPassword.length <= 32 && this.samePassword) {
                this.valid = true;
        }
    }

    public onSubmit(): void {
        this.checkValid();
        if (!this.samePassword) {
            Swal('新密码和确认密码不相同!', 'error');
        }
        if (!this.valid && this.samePassword) {
            Swal('密码长度应介于6和32之间,不应不填或过长. 过短');
        }
        if (this.valid) {
            this.authApiServer.ChangePassword(this.oldPassword, this.newPassword, this.confirmPassword)
            .pipe(catchError(error => {
                Swal('网络错误', '无法连接到XiangruiChat服务器.', 'error');
                return Promise.reject(error.message || error);
            }))
            .subscribe(result => {
                if (result.code === 0) {
                    Swal('设置成功', result.message, 'success');
                    this.router.navigate(['/settings']);
                } else {
                    Swal('请重试', result.message, 'error');
                }
            });
        }
    }
}
