import { Injectable } from '@angular/core';
import { versions } from '../../environments/versions';

@Injectable({
    providedIn: 'root'
})

export class CheckService {
    public checking = false;
    public version = versions.version;
    public revision = versions.revision;
    public buildTime = versions.buildTime;

    constructor() {}

    public checkVersion(checkButton: boolean): void {
        checkButton = !checkButton;
    }
}
